angular.module('app.services', [])

.factory('BlankFactory', [function(){

}])

//Esta es la funcion que compara las contraseñas y los usuarios. Ahora mismo solo compara "user" con su correspondiente contraseña "secret"
//Estos valores se tienen que resmpazar por la variable que se oobtiene del servidor dependiendo de cada usuario.
//Las promise se encargan de realizar una tarea y confirmar si esta se realizó dependiendo de su resultado. Al ser asincrona no tenemos que esperar una respuesta del resultado para continuar

.service('LoginService', function($q) {
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;
 
            if (name == 'user' && pw == 'secret') {
                deferred.resolve('Welcome ' + name + '!');
            } else {
                deferred.reject('Wrong credentials.');
            }
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
})

.service('SigninService', function($q) {
    return {
        signinUser: function(pw, c_pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;
 
            if (pw == c_pw) { //Aqui checa que las dos variables sean iguales, las de pw y la confirmacion c_pw
                deferred.resolve('Welcome!!');
            } else {
                deferred.reject('Password does not match');
            }
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
});



angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('tabsController.home', {
    url: '/',
    views: {
      'tab2': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('tabsController.workers', {
    url: '/workers/:category',
    views: {
      'tab2': {
        templateUrl: 'templates/workers.html',
        controller: 'workersCtrl'
      }
    }
  })

  .state('tabsController.listChat', {
    url: '/listchat',
    views: {
      'tab1': {
        templateUrl: 'templates/listChat.html',
        controller: 'listChatCtrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.chat'
      2) Using $state.go programatically:
        $state.go('tabsController.chat');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/listchat/chat/:id
      /page1/tab2/listchat/chat/:id
  */
  .state('tabsController.chat', {
    url: '/chat/:id',
    views: {
      'tab1': {
        templateUrl: 'templates/chat.html',
        controller: 'chatCtrl'
      },
      'tab2': {
        templateUrl: 'templates/chat.html',
        controller: 'chatCtrl'
      }
    }
  })

  .state('profile', {
    url: '/profile',
    templateUrl: 'templates/profile.html',
    controller: 'profileCtrl'
  })

  .state('signIn', {
    url: '/signin',
    templateUrl: 'templates/signIn.html',
    controller: 'signInCtrl'
  })

  .state('logIn', {
    url: '/login',
    templateUrl: 'templates/logIn.html',
    controller: 'logInCtrl'
  })

$urlRouterProvider.otherwise('/login')


});
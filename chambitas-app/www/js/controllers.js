angular.module('app.controllers', [])
  
.controller('menuCtrl', ['$scope', '$stateParams', '$window',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
      
.controller('homeCtrl', function ($log, $state, $window) {
	var vm = this;
	vm.filteredWorkers = [];
	vm.startChat=false;
	vm.user = $window.localStorage.getItem('user');
	vm.user = angular.fromJson(vm.user);
	vm.user = vm.user[0];
	vm.findWorkers = function(skill){
		vm.skill = skill;
		vm.clients = angular.fromJson(httpGet('Client'));
		for(var i=0;i<vm.clients.length;i++){
			if(vm.clients[i].workSkills){
				if(vm.clients[i].workSkills.indexOf(skill)>=0){
					vm.filteredWorkers.push(vm.clients[i]);
				}
			}
		}
	};
	vm.startChat = function(){
		$log.log("Start chat with",vm.client.name, ' for ',vm.skill);
		vm.startChat=!vm.startChat;
		vm.messages=angular.fromJson(httpGet("Message"));
		$log.log('Receiver',vm.messagesR);
		$log.log('Sender',vm.messagesS);
	};
	vm.sendMessage = function(){
		var newMessage ={
		    "message":vm.message,
		    "dateSend": new Date(),
		    "receiverId": vm.client.id,
		    "senderId": vm.user.id
		};
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "http://0163473.upweb.site:3000/api/Messages", true);
		xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		xhr.send(angular.toJson(newMessage));
		vm.messages.push(newMessage);
		vm.message='';
	};
	function httpGet(Model, filter='', variable=''){
    	//search for
    	var URL = "http://0163473.upweb.site:3000/api/";
    	URL += Model+"s";
    	if(filter!=''){
	    	URL += "?filter=%7B%22where%22%3A%7B%22"+filter+"%22%3A%22";
	    	URL += variable+"%22%7D%7D";
    	}
	    var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", URL, false ); // false for synchronous request
	    xmlHttp.send( null );
	    return xmlHttp.responseText;
	}
})

   
.controller('workersCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('listChatCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('chatCtrl', ['$scope', '$stateParams', '$location', '$log', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $location, $log) {
	var vm=this;
	

}])
   
.controller('profileCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

   
.controller('signInCtrl', ['$scope', '$stateParams', '$state','$log', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $log, $window) {
	var vm = this;
	vm.user = {};
	vm.user.workSkills=[];
	vm.works = ['Ayudante del hogar', 'Carpintería', 'Cerrajería', 'Construccion y decoracion', 'Electricista', 'Instalaciones','Jardinería','Plomería'];
	vm.addSkill = function(number){
		if(vm.user.workSkills.indexOf(vm.works[number])<0){
			vm.user.workSkills.push(vm.works[number]);
			$log.log(vm.user.workSkills);
		}else{
			vm.user.workSkills.splice(vm.user.workSkills.indexOf(vm.works[number]),1);
			$log.log(vm.user.workSkills);
		}
	};
	vm.register = function(){
		if(vm.confirm == vm.user.password){
			$log.log(vm.user);
			var xhr = new XMLHttpRequest();
			xhr.open("POST", "http://0163473.upweb.site:3000/api/Clients", true);
			xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
			xhr.send(angular.toJson(vm.user));
			$state.go('logIn');
		}
	};
}])
   
.controller('logInCtrl', ['$scope', '$stateParams', '$state', '$window',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $window) {
	var vm = this;
	vm.user = {};

    vm.login = function(){
		var user = httpGet("Client", "email", vm.user.email);
		//user = user[0];
		user = angular.fromJson(user);
		console.log(user[0]);
		if(user[0].password == vm.user.password){
			console.log("Bienvenido");
			$state.go('tabsController.home');
			$window.localStorage.clear();
			$window.localStorage.setItem('user',angular.toJson(user));
		}else{
			console.log("Datos incorrectos, vuelva a intentarlo");
		}
	}
	function httpGet(Model, filter='', variable=''){
    	//search for
    	var URL = "http://0163473.upweb.site:3000/api/";
    	URL += Model+"s";
    	if(filter!=''){
	    	URL += "?filter=%7B%22where%22%3A%7B%22"+filter+"%22%3A%22";
	    	URL += variable+"%22%7D%7D";
    	}
	    var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", URL, false ); // false for synchronous request
	    xmlHttp.send( null );
	    return xmlHttp.responseText;
	}
}])
 